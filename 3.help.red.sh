#!/bin/bash
echo -e "O comando abaixo exibe o nome do script que está sendo executado" 
echo $0
echo -e "O comando abaixo exibe o primeiro parâmetro passado"
echo $1
echo -e "O comando abaixo exibe o segundo parâmetro passado"
echo $2
echo -e "O comando abaixo exibe o número de parâmetros fornecidos"
echo $#
echo -e "O comando abaixo exibe todos os parâmetros separados por espaço em branco"
echo $*
echo -e "O comando abaixo retorna o Nome do Usuário"
echo -e $USER
echo -e "O comando abaixo retorna o Id do usuário"
echo -e $UID.
echo -e "O comando abaixo retorna o diretório atual."
echo $PWD
echo -e "O comando abaixo retorna o diretório home do usuário atual"
echo -e $HOME
echo -e "O comando abaixo retorna o path atual do sistema"
echo -e $PATH
echo -e "O comando abaixo retorna o número de linhas do terminal"
echo -e $LINES
echo -e "O comando abaixo retorna o número de colunas do terminl"
echo -e $COLUMNS
echo -e "O comando abaixo retorna a pilha de diretórios disponíveis para os comandos pushd e popd."
echo -e $DIRSTACK
echo -e "O comando abaixo retorna retorna informações sobre o GID."
echo -e $GROUPS
echo -e "O comando abaixo retorna o nome do computador"
echo -e $HOSTNAME
