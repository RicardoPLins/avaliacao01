#!/bin/bash
echo -e "> ou 1> redireciona para saída"
echo -e ">> redireciona para fim de arquivo"
echo -e "< redireciona para a entrada"
echo -e ">& ou 2> redireciona a saída de erros"
echo -e"| redireciona a saída de um comando para a entrada de um outro comando"
echo -e "exemplos de uso: "
echo -e "digite um diretório"
read dir
echo -e "digite um arquivo"
read arq1
ls $dir > $arq1
echo -e "o conteúdo do diretorio foi redirecionado e substituiu tudo do arq1"
cat $arq1
echo -e "digite um diretório"
read dir
ls $dir >> $arq1
echo -e 'o conteúdo do diretorio foi redirecionado e adicionará tudo no final do $arq1'
cat $arq1
echo -e "o conteúdo do arquivo criado vai ser mostrado utilizando um cat com <"
cat < $arq1
echo -e "Digite uma sequencia de 3 numeros"
read num1 num2 num3
echo -e "inorme um arquivo"
read x
echo -e "vai ser utilizado o | para redirecionar a saída com um comando para entrada de outro arquivo"
echo $num1 > $x 
echo $num2 >> $x
echo $num3 >> $x
cat $x | sort > numerosordenados.txt
echo -e "digite um dirétorio inexistente"
read dir2
echo -e "o conteúdo do erro sera redirecionado para um arquivo de erro"
touch erro.txt
ls dir2 >& erro.txt
